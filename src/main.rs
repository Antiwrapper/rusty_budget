extern crate rusty_budget;
#[macro_use] extern crate conrod;
extern crate find_folder;

mod main_loop;

fn main() {
	main_loop::run(main_loop::setup());
}
