use conrod;
use conrod::backend::glium::glium;
use conrod::glium::backend::glutin_backend::GlutinFacade;
use conrod::backend::glium::glium::{DisplayBuild, Surface};
use find_folder;
use std;

mod state_gui;

pub fn setup() -> (GlutinFacade, conrod::Ui) {
	const WIDTH: u32 = 720;
	const HEIGHT: u32 = 640;

	let window = glium::glutin::WindowBuilder::new()
		.with_vsync()
		.with_title("Money")
		.with_dimensions(WIDTH, HEIGHT)
		.with_multisampling(4)
		.build_glium()
		.unwrap();

	let mut ui = conrod::UiBuilder::new([WIDTH as f64, HEIGHT as f64])
		.theme(state_gui::theme())
		.build();

	let assets = find_folder::Search::KidsThenParents(3, 5).for_folder("assets").unwrap();
	let font_path = assets.join("fonts/NotoSans/NotoSans-Regular.ttf");
	ui.fonts.insert_from_file(font_path).unwrap();

	(window, ui)
}

pub fn run(windowui: (GlutinFacade, conrod::Ui)) {
	let (window, mut ui) = windowui;
	let mut renderer = conrod::backend::glium::Renderer::new(&window).unwrap();
	let ids = &mut state_gui::Ids::new(ui.widget_id_generator());
	let image_map = conrod::image::Map::<glium::texture::Texture2d>::new();

	// Poll events from the window.
	let mut last_update = std::time::Instant::now();
	let mut ui_needs_update = true;
	let mut state: state_gui::State = state_gui::State::new();
	'main: loop {

		// We don't want to loop any faster than 60 FPS, so wait until it has been at least
		// 16ms since the last yield.
		let sixteen_ms = std::time::Duration::from_millis(1);
		let duration_since_last_update = std::time::Instant::now().duration_since(last_update);
		if duration_since_last_update < sixteen_ms {
			std::thread::sleep(sixteen_ms - duration_since_last_update);
		}

		// Collect all pending events.
		let mut events: Vec<_> = window.poll_events().collect();

		// If there are no events and the `Ui` does not need updating, wait for the next event.
		if events.is_empty() && !ui_needs_update {
			events.extend(window.wait_events().next());
		}

		// Reset the needs_update flag and time this update.
		ui_needs_update = false;
		last_update = std::time::Instant::now();

		// Handle all events.
		for event in events {

			// Use the `winit` backend feature to convert the winit event to a conrod one.
			if let Some(event) = conrod::backend::winit::convert(event.clone(), &window) {
				ui.handle_event(event);
				ui_needs_update = true;
			}

			match event {
				// Cancel menus with Escape
				glium::glutin::Event::KeyboardInput(_, _, Some(glium::glutin::VirtualKeyCode::Escape))
				=> {
					if state.change_name {
						state.change_name = false;
						state.user.name = state.old_name.clone();
					}
					
					if state.change_birth {
						state.change_birth = false;
						state.user.dob_year = state.old_year;
						state.user.dob_month = state.old_month;
						state.user.dob_day = state.old_day;
					}
				},
				glium::glutin::Event::KeyboardInput(_, _, Some(glium::glutin::VirtualKeyCode::Return))
				=> {
					if state.change_birth {
						state.change_birth = false;
					}
				},
				glium::glutin::Event::Closed => break 'main,
				_ => {},
			}
		}

		state_gui::set_widgets(ui.set_widgets(), ids, &mut state);

		if let Some(primitives) = ui.draw_if_changed() {
			renderer.fill(&window, primitives, &image_map);
			let mut target = window.draw();
			target.clear_color(0.0, 0.0, 0.0, 1.0);
			renderer.draw(&window, &mut target, &image_map).unwrap();
			target.finish().unwrap();
		}
	}
}
