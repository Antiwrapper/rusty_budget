use conrod;
use conrod::color;
use std;
use rusty_budget::data;

mod gui;

pub fn theme() -> conrod::Theme {
	use conrod::position::{Align, Direction, Padding, Position, Relative};
	conrod::Theme {
		name: "Default Theme".to_string(),
		padding: Padding::none(),
		x_position: Position::Relative(Relative::Align(Align::Start), None),
		y_position: Position::Relative(Relative::Direction(Direction::Backwards, 20.0), None),
		background_color: conrod::color::BLUE,
		shape_color: color::rgba(1.0, 1.0, 1.0, 0.2),
		border_color: conrod::color::BLACK,
		border_width: 0.0,
		label_color: conrod::color::WHITE,
		font_id: None,
		font_size_large: 26,
		font_size_medium: 18,
		font_size_small: 12,
		widget_styling: std::collections::HashMap::new(),
		mouse_drag_threshold: 0.0,
		double_click_threshold: std::time::Duration::from_millis(500),
	}
}

widget_ids! {
	pub struct Ids {
		master,
		header,
		body,
		graph,
		master_scrollbar,
		tabs,
		tab_account,
		tab_user,
		tab_settings,

		title,
		subtitle,
		transactions,
		list_transactions,
		user_info,
		settings,
		active_account,
		cancel,
		create_account,
		edit_account,
		edit_account_value,
		add_transaction,
		edit_from_tran,
		change_name,
		change_birth,
		name_edit,
		birth_year_edit,
		birth_month_edit,
		birth_day_edit,
	}
}

pub struct State {
	pub temp_string: std::string::String,
	pub create_account: bool,
	pub add_transaction: bool,
	pub change_name: bool,
	pub change_birth: bool,
	pub user: data::User,
	pub active_account_index: usize,
	pub old_name: std::string::String,
	pub old_year: u64,
	pub old_month: u8,
	pub old_day: u8,
}

impl State {
	pub fn new() -> State {
		State {
			temp_string: std::string::String::new(),
			create_account: false,
			add_transaction: false,
			change_name: false,
			change_birth: false,
			user: data::User::new(),
			active_account_index: 0,
			old_name: std::string::String::new(),
			old_year: 1,
			old_month: 1,
			old_day: 1,
		}
	}
}

pub fn set_widgets(ref mut ui: conrod::UiCell, ids: &mut Ids, state: &mut State) {
	gui::create_canvas(ui, ids);
	gui::create_tabs(ui, ids);
	gui::create_header(ui, ids);
	gui::logic(ui, ids, state);
}
