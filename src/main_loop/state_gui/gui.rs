use conrod;
use conrod::{color, widget, Colorable, Labelable, Positionable, Sizeable, Widget};
use std;
use main_loop::state_gui::{Ids, State};
use rusty_budget::data;

fn text (text: widget::Text) -> widget::Text {
	text.color(color::WHITE).font_size(36)
}

pub fn create_canvas(ref mut ui: &mut conrod::UiCell, ids: &mut Ids) {
	widget::Canvas::new().flow_down(&[
		(ids.header, widget::Canvas::new().color(color::BLUE).pad_bottom(20.0).length(130.0)),
		(ids.body, widget::Canvas::new().flow_right(&[
			(ids.graph, widget::Canvas::new().color(color::LIGHT_BLUE).pad(20.0)),
		])),
		]).scroll_kids_vertically().set(ids.master, ui);

	widget::Scrollbar::y_axis(ids.master)
		.auto_hide(true)
		.set(ids.master_scrollbar, ui);
}

pub fn create_tabs(ref mut ui: &mut conrod::UiCell, ids: &mut Ids) {
	widget::Tabs::new(&[
		(ids.tab_account, "Account"),
		(ids.tab_user, "User"),
		(ids.tab_settings, "Settings")])
		.starting_canvas(ids.tab_user)
		.wh_of(ids.graph)
		.color(color::LIGHT_BLUE)
		.label_color(color::WHITE)
		.middle_of(ids.graph)
		.set(ids.tabs, ui);
}

pub fn create_header(ref mut ui: &mut conrod::UiCell, ids: &mut Ids) {
	widget::Text::new("Rusty Budget")
		.color(color::WHITE)
		.font_size(48)
		.middle_of(ids.header)
		.set(ids.title, ui);
	widget::Text::new("A personal budget program written in rust")
		.color(color::GREY)
		.mid_bottom_of(ids.header)
		.set(ids.subtitle, ui);
}

pub fn logic(ref mut ui: &mut conrod::UiCell, ids: &mut Ids, state: &mut State) {
	let num_accounts = state.user.accounts.len();
	if num_accounts == 0 {
		text(widget::Text::new(format!("No accounts for {} available", state.user.name).as_ref()))
			.mid_top_with_margin_on(ids.tab_account, 64.0)
			.set(ids.transactions, ui);

		let button = widget::Button::new()
			.mid_bottom_with_margin_on(ids.transactions, -160.0)
			.w_h(256.0, 128.0)
			.label("Create new account");

		for _click in button.set(ids.create_account, ui) {
			state.create_account = true;
			state.temp_string = std::string::String::new();
			state.user.accounts.push(data::Account::new());
		}
	} else if state.create_account {
		for edit in widget::TextBox::new(state.user.accounts[num_accounts - 1].name.as_ref())
			.mid_top_with_margin_on(ids.tab_account, 64.0)
			.w_h(200.0, 64.0)
			.set(ids.edit_account, ui) {

			match edit {
				conrod::widget::text_box::Event::Update(update) => state.user.accounts[num_accounts - 1].name = update,
				conrod::widget::text_box::Event::Enter => state.create_account = false,
			}
		}

		for edit in widget::TextBox::new(state.temp_string.as_ref())
			.down_from(ids.edit_account, 8.0)
			.w_h(200.0, 64.0)
			.set(ids.edit_account_value, ui) {

			match edit {
				conrod::widget::text_box::Event::Update(update) => {
						state.user.accounts[num_accounts - 1].value = match update.parse() {
							Ok(result) => result,
							_ => state.user.accounts[num_accounts - 1].value,
						};

						state.temp_string = update;
				},
				conrod::widget::text_box::Event::Enter => state.create_account = false,
			}
		}

		let button = widget::Button::new()
			.w_h(128.0, 64.0)
			.label("Cancel");
		
		for _click in button
			.parent(ids.tab_account)
			.right_from(ids.edit_account, 8.0)
			.set(ids.cancel, ui) {

			state.user.accounts.pop();
			state.create_account = false;
		}
	} else if state.add_transaction {
		widget::Text::new(state.user.accounts[state.active_account_index].name.as_ref())
			.bottom_left_with_margins_on(ids.header, -8.0, 8.0)
			.set(ids.active_account, ui);

		let ref mut account = state.user.accounts[state.active_account_index];
		let index = account.transactions.len() - 1;

		for edit in widget::TextBox::new(account.transactions[index].from.as_ref())
			.mid_top_with_margin_on(ids.tab_account, 64.0).w_h(200.0, 64.0)
			.set(ids.edit_from_tran, ui) {

			match edit {
				conrod::widget::text_box::Event::Update(update) => account.transactions[index].from = update,
				conrod::widget::text_box::Event::Enter => state.add_transaction = false,
			}
		}

		let button = widget::Button::new()
			.w_h(128.0, 64.0)
			.label("Cancel");
		
		for _click in button
			.parent(ids.tab_account)
			.right_from(ids.edit_from_tran, 8.0)
			.set(ids.cancel, ui) {

			account.transactions.pop();
			state.add_transaction = false;
		}
	} else {
		widget::Text::new(state.user.accounts[state.active_account_index].name.as_ref())
			.bottom_left_with_margins_on(ids.header, -8.0, 8.0)
			.set(ids.active_account, ui);
		
		if state.user.accounts[state.active_account_index].transactions.len() == 0 {
			text(widget::Text::new("No transactions found"))
				.mid_top_with_margin_on(ids.tab_account, 64.0)
				.set(ids.transactions, ui);
		} else {
			text(widget::Text::new("Transactions:"))
				.center_justify()
				.mid_top_with_margin_on(ids.tab_account, 32.0)
				.set(ids.transactions, ui);

			let (mut items, list_scrollbar) = widget::List::new(state.user.accounts[state.active_account_index].transactions.len(), 48.0)
				.scrollbar_on_top()
				.w_h(400.0, 252.0)
				.mid_bottom_with_margin_on(ids.transactions, -260.0)
				.set(ids.list_transactions, ui);

			while let Some(item) = items.next(ui) {
				let i = item.i;
				let ref account = state.user.accounts[state.active_account_index];
				let label = format!("{}: {}", account.transactions[i].from, account.transactions[i].value);
				let transaction = widget::Button::new()
					.label(&label);
				item.set(transaction, ui);
			}

			if let Some(s) = list_scrollbar {s.set(ui)}
		}

		let button = widget::Button::new()
			.mid_right_with_margin_on(ids.transactions, -264.0)
			.w_h(256.0, 64.0)
			.label("Add transaction");

		for _click in button.set(ids.add_transaction, ui) {
			state.add_transaction = true;
			state.user.accounts[state.active_account_index].transactions.push(data::Transaction::new());
		}
	}
	
	text(widget::Text::new("Fullscreen: no\nGraphics: Crysis Level\nReport user activity to mothership: yes"))
		.middle_of(ids.tab_settings)
		.set(ids.settings, ui);
	
	if state.change_name {
		for edit in widget::TextBox::new(state.user.name.as_ref())
			.mid_top_with_margin_on(ids.tab_user, 64.0).w_h(200.0, 64.0)
			.set(ids.name_edit, ui) {

			match edit {
				conrod::widget::text_box::Event::Update(update) => state.user.name = update,
				conrod::widget::text_box::Event::Enter => state.change_name = false,
			}
		}

		let button = widget::Button::new()
			.w_h(128.0, 64.0)
			.label("Cancel");
		
		for _click in button
			.parent(ids.tab_user)
			.right_from(ids.name_edit, 8.0)
			.set(ids.cancel, ui) {

			state.user.name = state.old_name.clone();
			state.change_name = false;
		}
	} else if state.change_birth {
		for year in widget::NumberDialer::new(state.user.dob_year as f64, 1900.0, 2017.0, 0)
			.mid_top_with_margin_on(ids.tab_user, 64.0)
			.w_h(200.0, 64.0)
			.label("Year")
			.set(ids.birth_year_edit, ui) {

			state.user.dob_year = year as u64;
		}

		for month in widget::NumberDialer::new(state.user.dob_month as f64, 1.0, 12.0, 0)
			.parent(ids.tab_user)
			.down_from(ids.birth_year_edit, 8.0)
			.w_h(200.0, 64.0)
			.label("Month")
			.set(ids.birth_month_edit, ui) {

			state.user.dob_month = month as u8;
		}

		for day in widget::NumberDialer::new(state.user.dob_day as f64, 1.0, 31.0, 0)
			.parent(ids.tab_user)
			.down_from(ids.birth_month_edit, 8.0)
			.w_h(200.0, 64.0)
			.label("Day")
			.set(ids.birth_day_edit, ui) {

			state.user.dob_day = day as u8;
		}

		let button = widget::Button::new()
			.w_h(128.0, 64.0)
			.label("Cancel");

		for _click in button
			.parent(ids.tab_user)
			.align_top_of(ids.birth_year_edit)
			.right_from(ids.birth_year_edit, 8.0)
			.set(ids.cancel, ui) {

			state.user.dob_year = state.old_year;
			state.user.dob_month = state.old_month;
			state.user.dob_day = state.old_day;
			state.change_birth = false;
		}
	} else {
		text(widget::Text::new(format!("Name: {}\nAge: {}", state.user.name, 2017 - state.user.dob_year).as_ref()))
			.mid_top_with_margin_on(ids.tab_user, 64.0)
			.line_spacing(1.5)
			.set(ids.user_info, ui);

		let button = widget::Button::new()
			.w_h(48.0, 32.0)
			.label("Edit");

		for _click in button
			.clone()
			.parent(ids.tab_user)
			.right_from(ids.user_info, 32.0)
			.down_from(ids.user_info, -64.0)
			.set(ids.change_name, ui) {

			state.old_name = state.user.name.clone();
			state.change_name = true;
		}

		for _click in button
			.clone()
			.parent(ids.tab_user)
			.right_from(ids.user_info, 32.0)
			.down_from(ids.user_info, -24.0)
			.set(ids.change_birth, ui) {
			
			state.old_year = state.user.dob_year;
			state.old_month = state.user.dob_month;
			state.old_day = state.user.dob_day;
			state.change_birth = true;
		}
	}
}
