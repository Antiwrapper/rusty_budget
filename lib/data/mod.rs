use std::string::String;

pub struct Account {
	pub name: String,
	pub value: f64,
	pub deposit: f64,
	pub transactions: Vec<Transaction>,
}

pub struct User {
	pub name: String,
	pub accounts: Vec<Account>,
	pub dob_year: u64,
	pub dob_month: u8,
	pub dob_day: u8,
}

pub struct Transaction {
	pub from: String,
	pub value: f64,
}

impl Account {
	pub fn new() -> Account {
		Account {
			name: String::from("Bank account"),
			value: 0.0,
			deposit: 0.0,
			transactions: vec![],
		}
	}
}

impl User {
	pub fn new() -> User {
		User {
			name: String::from("Bob"),
			accounts: vec![],
			dob_year: 1999,
			dob_month: 1,
			dob_day: 1,
		}
	}
}

impl Transaction {
	pub fn new() -> Transaction {
		Transaction {
			from: String::new(),
			value: 0.0,
		}
	}
}
